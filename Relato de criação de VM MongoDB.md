# Instalando o MongoDB no Ubuntu

Não é alvo deste curso ensinar a instalar o MongoDB. Criei uma máquina virtual com Ubuntu 18, Apache2, PHP7.2 e MongoDB instalados. Esta máquina está disponibilizada para download e uso imediato. 

O usuário padrão é *mongo* com senha *m0ngo* 

Todavia, se quiser fazer sua própria instalação aqui estão os passos:

Primeiro preparei para atualizar o sistema

```bash
$ sudo nano /etc/apt/sources.list.d/sid.list
```

```ini
deb http://deb.debian.org/debian/ sid main
deb-src http://deb.debian.org/debian/ sid main
```

Adicionei as chaves que vou precisar na instalação do MongoDB

```bash
$ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 04EE7237B7D453EC
$ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 648ACFD622F3D138
$ sudo apt update
$ sudo apt install mongodb-server
$ sudo mkdir /data/db
$ sudo chown mongo.mongo /data/db
$ sudo systemctl enable mongodb
```

Criei um usuário *convidado* para poder fazer os testes

```bash
$ mongo
> db.createUser({ user:"convidado", pwd: "abc123", roles: [ { role: "userAdminAnyDatabase", db: "admin" }]})
```

Liberei o acesso ao mongo pela rede

```bash
$ sudo nano /etc/mongodb.conf
```

Mudei a linha

```ini
bind_ip = 0.0.0.0
```

##### Instalando uma ferramenta GUI para acessar mongo

https://robomongo.org/download

##### Instalando PHP e Apache2

```bash
$ sudo apt-get install -y php-pear
$ sudo apt-get install -y php-dev pkg-config 
$ sudo pecl install mongodb
$ sudo apt-get install -y php apache2
```

Modifiquei os arquivos */etc/php/7.2/cli/php.ini*  e */etc/php/7.2/apache2/php.ini* adicionando a seguinte linha em cada um deles:

```ini
extension=mongodb.so
```

Criei um pequeno script para verificar se tudo está instalado conforme esperado */var/www/html/info.php*

```php
<?php
  phpinfo();
?>
```

Reiniciei o apache e habilitei o seviço

```bash
$ sudo systemctl start apache2
$ sudo systemctl enable apache2
```

Testei usando o navegador: http://192.168.56.7/info.php comprovando que a extensão mongodb.so foi carregada.

Criei um script para testar a configuração do mongo e sua disponibilidade junto ao php chamado /var/www/html/testar-mongo.php

```php
<?php
  $manager = new \MongoDB\Driver\Manager();
  print '<pre>';
  print_r($manager);
  print '</pre>';
?>
```

Instalei o serviço de arquivos *samba* para podermos acessar os arquivos desde o hospedeiro sem muita complicação

```bash
$ sudo apt install samba -y
```

Adicionei o usuário *mongo* à lista de usuários *samba*

```bash
$ sudo smbpasswd -a mongo
```

mantendo a mesma senha do serviço *ssh* apenas para simplificação (em produção isso não deve ser feito assim. Aliás, nem usaríamos *samba* para servirmos os arquivos)

Com isso feito, você pode acessar seu servidor desde seu navegador de arquivos. Digamos que o endereço de sua máquina virtual é 192.168.56.90, então seria assim:

smb://192.168.56.90 no Linux

ou Iniciar | Executar | \\\\192.168.56.90 no Windows

Para facilitar, na hora de digitar usuário e senha, diga para seu sistema operacional lembrar elas.

Depois disso tive que remanejar o apache para servir os arquivos desde a pasta do usuário *mongo*. Alterei o arquivo /etc/apache2/apache2.conf adicionando a seguinte secção e linha:

```ini
<Directory /home/mongo/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>

ServerName mongo-db1
```

Alterei a definição da pasta principal do apache no arquivo /etc/apache2/sites-enabled/000-default.conf mudando apenas ServerName e DocumentRoot:

```ini
<VirtualHost *:80>
	ServerName mongo-db1
	DocumentRoot /home/mongo/www
</VirtualHost>
```

E mudei o samba para que os arquivos gravados por meio dele, pudessem ser acessados pelo *apache*.

```ini
[homes]
   comment = Pastas do usuário
   browseable = no
   read only = no
```

Finalmente reiniciei tudo para observar o funcionamento.
