<?php
  /* Conectamos ao banco de dados no servidor local */
  $mc = new \MongoDB\Driver\Manager("mongodb://localhost:27017");

  /* Preparamos uma consulta */
  $query = new MongoDB\Driver\Query([]);

  /* Executamos a consulta sobre a conexão */
  $rows = $mc->executeQuery("biblioteca.livros", $query);

  /* para facilitar a saída via HTML, abrimos um bloco PRE */
  echo "<pre>";

  /* criamos um contador de linhas apenas para fim pedagógico */
  $linha=0;

  /* percorremos os resultados */
  foreach ($rows as $row) {

    /* incrementamos o contador de linhas */
    $linha++;

    /* mostramos a linha em tela */
    echo "$linha\n";

    /* imprimimos cada um dos campos que virem da consulta */
    foreach ($row as $key => $value) {
      echo "\t$key: $value\n";
    }
  }

  /* encerramos o bloco PRE */
  echo "</pre>";
?>