<?php
  echo "<pre>";
  try {
    $mc = new \MongoDB\Driver\Manager();
    $query = new MongoDB\Driver\Query([]);
    $rows = $mc->executeQuery("biblioteca.livros", $query);
    $linha=0;
    foreach ($rows as $row) {
      $linha++;
      echo "$linha\n";
      foreach ($row as $key => $value) {
        echo "\t$key: $value\n";
      }
    }
  } catch (MongoDB\Driver\Exception\Exception $e) {
    echo "Exceção:", $e->getMessage(), "\n";
  }
  echo "</pre>";
?>