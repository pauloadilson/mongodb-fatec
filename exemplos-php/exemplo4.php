<?php
  $mc = new \MongoDB\Driver\Manager();

  $filtro = [ 'estoque' => [ "$gt" => 2 ] ];

  $query = new MongoDB\Driver\Query($filtro);
  $rows = $mc->executeQuery("biblioteca.livros", $query);
  echo "<pre>";
  $linha=0;
  foreach ($rows as $row) {
    $linha++;
    echo "$linha\n";
    foreach ($row as $key => $value) {
      echo "\t$key: $value\n";
    }
  }
  echo "</pre>";
?>